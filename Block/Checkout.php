<?php

namespace Imoje\Paywall\Block;

include_once __DIR__ . "/../../imoje-libs-module/PaymentCore/autoload.php";

use Imoje\Payment\Payment;
use Imoje\Payment\Util;
use Imoje\Paywall\Model\Order;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Api\Data\StoreInterface;

/**
 * Class Checkout
 *
 * @package Imoje\Paywall\Block
 */
class Checkout extends Template
{

	/**
	 * @var \Magento\Framework\UrlInterface
	 */
	public $urlBuilder;

	/**
	 * @var \Magento\Framework\App\Config\ScopeConfigInterface
	 */
	protected $scopeConfig;

	/**
	 * @var Registry
	 */
	private $registry;

	/**
	 * @var Order
	 */
	private $order;

	/**
	 * @var StoreInterface
	 */
	private $store;

	/**
	 * @var Cart
	 */
	private $cartHelper;

	/**
	 * @var ResponseFactory
	 */
	private $responseFactory;

	/**
	 * @var \Magento\Framework\App\ProductMetadataInterface $productMetadataInterface
	 */
	private $productMetadataInterface;

	/**
	 * Checkout constructor.
	 *
	 * @param Context                                            $context
	 * @param Order                                              $order
	 * @param Registry                                           $registry
	 * @param StoreInterface                                     $store
	 * @param \Magento\Framework\UrlInterface                    $urlBuilder
	 * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
	 * @param \Magento\Checkout\Helper\Cart                      $cartHelper
	 * @param \Magento\Framework\App\ResponseFactory             $responseFactory
	 * @param \Magento\Framework\App\ProductMetadataInterface    $productMetadataInterface
	 *
	 * @internal param OrderFactory $orderFactory
	 */
	public function __construct(
		Context $context,
		Order $order,
		Registry $registry,
		StoreInterface $store,
		\Magento\Framework\UrlInterface $urlBuilder,
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
		\Magento\Checkout\Helper\Cart $cartHelper,
		\Magento\Framework\App\ResponseFactory $responseFactory,
		\Magento\Framework\App\ProductMetadataInterface $productMetadataInterface

	) {
		parent::__construct($context);
		$this->registry = $registry;
		$this->order = $order;
		$this->store = $store;
		$this->urlBuilder = $urlBuilder;
		$this->scopeConfig = $scopeConfig;
		$this->cartHelper = $cartHelper;
		$this->responseFactory = $responseFactory;
		$this->productMetadataInterface = $productMetadataInterface;

		if($this->getOrderState($this->getOrderIdFromPost()) === \Magento\Sales\Model\Order::STATE_PENDING_PAYMENT) {

			$this->redirectToHomePage();
			die();
		}
	}

	/**
	 * That function redirect to homepage
	 *
	 * @return mixed
	 */
	public function redirectToHomePage()
	{
		return $this->responseFactory->create()->setRedirect($this->urlBuilder->getUrl())->sendResponse();
	}

	/**
	 * @return string
	 */
	public function getEnvironment()
	{
		return $this->getConfigValue('payment/imoje_paywall/sandbox') == 1
			? Util::ENVIRONMENT_SANDBOX
			: Util::ENVIRONMENT_PRODUCTION;
	}

	/**
	 * @return array
	 */
	public function getPaymentMethods()
	{

		return json_decode($this->getConfigValue('payment/imoje_paywall/payment_methods'), true);
	}

	/**
	 * @param string $orderId
	 *
	 * @return mixed
	 */
	public function getOrderState($orderId = '')
	{
		return $this->getOrderObject($orderId)->getState();
	}

	/**
	 * @param string $orderId
	 *
	 * @return string
	 */
	public function renderForm($orderId = '')
	{

		if(empty($orderId)) {
			$orderId = $this->getOrderId();
		}

		$order = $this->getOrderObject($orderId);

		$order->setState(\Magento\Sales\Model\Order::STATE_PENDING_PAYMENT);
		$order->save();

		/**
		 * Get Magento version.
		 */
		$version = $this->productMetadataInterface->getVersion();

		$payment = new Payment(
			$this->getEnvironment(),
			$this->getConfigValue('payment/imoje_paywall/service_key'),
			$this->getConfigValue('payment/imoje_paywall/service_id'),
			$this->getConfigValue('payment/imoje_paywall/merchant_id')
		);

		$orderData = Util::prepareData(
			Util::convertAmountToFractional($order->getGrandTotal()),
			$order->getOrderCurrency()->getCode(),
			$order->getIncrementId(),
			'',
			$order->getBillingAddress()->getFirstname(),
			$order->getBillingAddress()->getLastname(),
			$order->getCustomerEmail(),
			$order->getBillingAddress()->getTelephone(),
			$this->urlBuilder->getUrl('checkout/onepage/success/'),
			$this->urlBuilder->getUrl('checkout/onepage/failure/'),
			$this->urlBuilder->getUrl('checkout/onepage/failure/'),
			$this->getConfigValue('payment/imoje_paywall/version') . ';magento23_' . $version
		);

		return $payment->buildOrderForm($orderData, __('Continue'));
	}

	/**
	 * @return integer
	 */
	public function getOrderId()
	{
		return $this->registry->registry('orderId');
	}

	/**
	 * Render form with order information prepared for Payment Page
	 *
	 * @param string $id
	 *
	 * @return bool|\Imoje\Paywall\Model\Sales\Order
	 */
	public function getOrderObject($id = '')
	{
		if(empty($id)) {
			$id = $this->getOrderId();
		}

		return $this->order->loadOrderById($id);
	}

	/**
	 * @return string
	 */
	public function getOrderIdFromPost()
	{
		$id = '';
		if(isset($_POST['orderId'])) {
			$id = $_POST['orderId'];
		}

		return $id;
	}

	/**
	 * @param string $value
	 *
	 * @return string
	 */
	private function getConfigValue($value)
	{
		return $this->scopeConfig->getValue($value, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}
}
