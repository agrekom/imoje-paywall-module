<?php

namespace Imoje\Paywall\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Payment\Helper\Data;

/**
 * Class ConfigProvider
 *
 * @package Imoje\Paywall\Model
 */
class ConfigProvider implements ConfigProviderInterface
{
	const PP_CODE = 'imoje_paywall';

	/**
	 * @var \Imoje\Paywall\Model\Paywall
	 */
	protected $paymentPageMethod;

	/**
	 * Payment ConfigProvider constructor.
	 *
	 * @param Data $paymentHelper
	 *
	 * @throws \Magento\Framework\Exception\LocalizedException
	 */
	public function __construct(Data $paymentHelper)
	{
		$this->paymentPageMethod = $paymentHelper->getMethodInstance(self::PP_CODE);
	}

	/**
	 * Retrieve assoc array of checkout configuration
	 *
	 * @return array
	 */
	public function getConfig()
	{
		$config = [];

		if($this->paymentPageMethod->isAvailable()) {
			$config = [
				'payment' => [
					self::PP_CODE => [
						'redirectUrl'    => $this->paymentPageMethod->getCheckoutRedirectUrl(),
						'paymentLogoSrc' => $this->paymentPageMethod->getPaymentLogoSrc(),
					],
				],
			];
		}

		return $config;
	}
}
