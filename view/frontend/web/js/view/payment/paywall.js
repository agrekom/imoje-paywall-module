/*browser:true*/
/*global define*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (Component,
              rendererList) {
        'use strict';
        rendererList.push(
            {
                type: 'imoje_paywall',
                component: 'Imoje_Paywall/js/view/payment/method-renderer/paywall'
            }
        );
        return Component.extend({});
    }
);
